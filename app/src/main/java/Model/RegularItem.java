package Model;

import android.os.Parcelable;

public interface RegularItem extends Parcelable {
    String getName();
    String getPrice();
}
