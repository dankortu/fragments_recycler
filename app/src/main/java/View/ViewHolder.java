package View;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import Model.RegularItem;
import com.example.fragmentsrecycler.databinding.ItemBinding;

public class ViewHolder<M extends RegularItem> extends RecyclerView.ViewHolder { //View

    private final ItemBinding binding;
    private final Adapter.OnItemClickCallback<M> callback;
    private M currentItem;

    public ViewHolder(@NonNull ItemBinding itemView, @NonNull Adapter.OnItemClickCallback<M> callback) {
        super(itemView.getRoot());
        binding = itemView;
        this.callback = callback;
    }

    public void bind(M item) {
        this.currentItem = item;
        binding.getRoot().setOnClickListener(this::handleClick);
        binding.name.setText(item.getName());
        binding.price.setText(item.getPrice());
    }

    private void handleClick(View view) {
        itemView.postDelayed(() -> callback.onClick(currentItem, getAdapterPosition()), 250);
        callback.onClick(currentItem, getAdapterPosition());
    }
}