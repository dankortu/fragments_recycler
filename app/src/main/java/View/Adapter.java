package View;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import Model.RegularItem;
import com.example.fragmentsrecycler.databinding.ItemBinding;

import java.util.ArrayList;
import java.util.List;

public class Adapter<M extends RegularItem> extends RecyclerView.Adapter<ViewHolder<M>> { // View
    private final List<M> items = new ArrayList<>();
    private final OnItemClickCallback<M> callback;

    public Adapter(@NonNull OnItemClickCallback<M> callback) {
        this.callback = callback;
    }


    public void setItems(List<M> items) {
        this.items.clear();
        this.items.addAll(items);

        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    @NonNull
    @Override
    public ViewHolder<M> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new ViewHolder<>(ItemBinding.inflate(inflater, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder<M> holder, int position) {
        M item = items.get(position);
        holder.bind(item);
    }


    public interface OnItemClickCallback<M> {
        void onClick(M item, int adapterPosition);
    }
}
