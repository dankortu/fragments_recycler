package View;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import Model.Item;

import com.example.fragmentsrecycler.R;
import Model.RegularItem;

import java.util.ArrayList;
import java.util.List;


public class MyFragment extends Fragment { // View

    private static final String TAG = "MyFragment";

    private static final String ITEMS_KEY = "items";
    private static final String NOTIFY_LISTENERS_ARRAY = "notifyArray";
    private static final String RESULT_LISTENER_ID_KEY = "resultListenerId";

    private Adapter<RegularItem> adapter;
    private List<RegularItem> regularItems;

    public static MyFragment newInstance(RegularItem[] items,
                                         String[] notifyListenersArray,
                                         String resultListenerId) {
        final Bundle args = new Bundle();
        MyFragment fragment = new MyFragment();
        switch (resultListenerId) {
            case "input":
                fragment = new InputFragment();
                break;

            case "middle":
                fragment = new MiddleFragment();
                break;

            case "output":
                fragment = new OutputFragment();
                break;
        }

        args.putParcelableArray(ITEMS_KEY, items);
        args.putStringArray(NOTIFY_LISTENERS_ARRAY, notifyListenersArray);
        args.putString(RESULT_LISTENER_ID_KEY, resultListenerId);

        fragment.setArguments(args);
        return fragment;
    }

    public MyFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Bundle arguments = getArguments();
        if (arguments != null) {
            initFragment(arguments);
        }
    }

    private void initFragment(Bundle arguments) {
        regularItems = new ArrayList<>();
        for (Parcelable parcelable : arguments.getParcelableArray(ITEMS_KEY)) {
            regularItems.add((RegularItem) parcelable);
        }

        final String[] notifyListenersArray = arguments.getStringArray(NOTIFY_LISTENERS_ARRAY);

        adapter = new Adapter<>((item, adapterPosition) -> {
            if (isAdded()) {
                regularItems.remove(item);

                final Bundle result = new Bundle();
                result.putString("name", item.getName());
                result.putString("price", item.getPrice());

                for (String notifyId : notifyListenersArray) {
                    getParentFragmentManager().setFragmentResult(notifyId, result);
                }

                adapter.setItems(regularItems);
            }
        });

        adapter.setItems(regularItems);

        final String resultListenerId = arguments.getString(RESULT_LISTENER_ID_KEY);
        if (resultListenerId != null) {
            getParentFragmentManager().setFragmentResultListener(
                    resultListenerId,
                    this,
                    (requestKey, result) -> {
                        String name = result.getString("name");
                        String price = result.getString("price");

                        for (RegularItem item : regularItems) {
                           String name1 = item.getName();
                           String price1 = item.getPrice();
                           if (name.equals(name1) && price.equals(price1)) {
                               regularItems.remove(item);
                           }
                        }
                        regularItems.add(new Item(name, price));
                        adapter.setItems(regularItems);
                    }
            );
        } else {
            Log.w(TAG, "initFragment: resultListenerId is null");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        RecyclerView recycler = view.findViewById(R.id.recycler);
        recycler.setAdapter(adapter);
        recycler.setLayoutManager(new LinearLayoutManager(requireContext()));
    }

}