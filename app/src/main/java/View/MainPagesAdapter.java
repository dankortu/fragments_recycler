package View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MainPagesAdapter extends FragmentPagerAdapter { //View

    private final List<Fragment> initialFragments;

    public MainPagesAdapter(@NonNull FragmentManager fm, int behaviour, List<Fragment> fragmentsForAdapter) {
        super(fm, behaviour);

        this.initialFragments = fragmentsForAdapter;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
            return Objects.requireNonNull(initialFragments.get(position));
    }

    @Override
    public int getCount() {
        return initialFragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return String.valueOf(position);
    }
}
