package com.example.fragmentsrecycler;

import android.content.res.Configuration;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;

import com.example.fragmentsrecycler.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Model.Item;
import Model.RegularItem;
import View.MainPagesAdapter;
import View.MyFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final List<Fragment> fragments = new ArrayList<>(getSupportFragmentManager().getFragments());
        super.onCreate(savedInstanceState);
        final ActivityMainBinding viewBinding = ActivityMainBinding.inflate(getLayoutInflater());

        setContentView(viewBinding.getRoot());

        if (fragments.isEmpty()) {

            MyFragment inputFragment = MyFragment.newInstance(new RegularItem[]{new Item("butter", "20"), new Item("apple", "23"), new Item("mango", "44")}, new String[]{"middle"}, "input");
            MyFragment middleFragment = MyFragment.newInstance(new RegularItem[]{}, new String[]{"input", "output"}, "middle");
            MyFragment outputFragment = MyFragment.newInstance(new RegularItem[]{new Item("bread", "15"), new Item("sausage", "12"), new Item("juice", "22")}, new String[]{"middle"}, "output");

            fragments.addAll(Arrays.asList(inputFragment, middleFragment,outputFragment));
        }

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            MainPagesAdapter adapter = new MainPagesAdapter(
                    getSupportFragmentManager(),
                    FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT,
                   fragments
            );


            viewBinding.viewPager.setAdapter(adapter);
            viewBinding.tabLayout.setupWithViewPager(viewBinding.viewPager);
        } else {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.addToBackStack(null);
            transaction.replace(R.id.first, fragments.get(0)).replace(R.id.second, fragments.get(1)).replace(R.id.third, fragments.get(2)).commit();
        }

    }

}

